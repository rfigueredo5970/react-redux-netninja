import React from 'react';

const Contact = (props) => {
    // setTimeout(() => {
    //     props.history.push('/about');
    // }, 2000);

    return (
        <div className="container">
            <h4 className="center">Contact</h4>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores cupiditate ut ex, 
            excepturi, velit reprehenderit nostrum eveniet labore porro quos voluptates alias 
            dicta soluta odio maxime perferendis beatae voluptate? Iusto?</p>
        </div>
    );
}

export default Contact;
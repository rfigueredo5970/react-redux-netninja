import React from 'react';
import Rainbow from '../hoc/Rainbow'

const About = (props) => {
    return (
        <div className="container">
            <h4 className="center">About</h4>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores cupiditate ut ex, 
            excepturi, velit reprehenderit nostrum eveniet labore porro quos voluptates alias 
            dicta soluta odio maxime perferendis beatae voluptate? Iusto?</p>
        </div>
    );
}

export default Rainbow(About);